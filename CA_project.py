import os
import streamlit as st
import urllib
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "C:/Users/LENOVO PC/Downloads/qwiklabs-gcp-04-d1bbb64f28a3-891e3b96c684.json"

from google.cloud import texttospeech
#from google.cloud import translate
from google.cloud import translate_v2 as translate

##Convert one language to another.

#@st.cache
#Lang_List = translate_client.get_languages()


with st.container():
    try:

        st.title('Speech Translator')
        
        translate_client = translate.Client()
        
        text = st.text_input("Enter Text")
        
        st.write("Original Text : {}".format(text))
        
        tar_lang = st.selectbox("Choose Target Language", ['en', 'fr', 'hi', 'es'])
        lang_code = {'en' : 'en-US',
                     'fr' : 'fr-CA',
                     'hi' : 'hi-IN', 
                     'es' : 'es-ES'
                    }
        
        #Translate Text
        output = translate_client.translate(text, target_language=tar_lang, model='base')
        st.write("Detected Source Language : {}".format(output["detectedSourceLanguage"]))
        translated_text = output["translatedText"]
        
        st.write("Translated Text : {}".format(translated_text))
       
        
        #Convert Translated Text to Speech

        # Instantiates a client
        tts_client = texttospeech.TextToSpeechClient()

        # Set the text input to be synthesized
        synthesis_input = texttospeech.SynthesisInput(text = translated_text)

        # Build the voice request, select the language code ("en-US") and the ssml
        # voice gender ("neutral")
        voice = texttospeech.VoiceSelectionParams(
            language_code=lang_code[tar_lang], ssml_gender=texttospeech.SsmlVoiceGender.NEUTRAL
        )

        # Select the type of audio file you want returned
        audio_config = texttospeech.AudioConfig(
            audio_encoding=texttospeech.AudioEncoding.MP3
        )

        # Perform the text-to-speech request on the text input with the selected
        # voice parameters and audio file type
        response = tts_client.synthesize_speech(
            input=synthesis_input, voice=voice, audio_config=audio_config
        )

        # The response's audio_content is binary.
        with open("speech.mp3", "wb") as out:
            # Write the response to the output file.
            out.write(response.audio_content)
            
        #Displaying Audio
        st.subheader("Play Audio File")
        st.audio(response.audio_content, format="audio/mp3", start_time=0)
     
       
    except urllib.error.URLError as e:
        st.error(
            """
            *This demo requires internet access.*

            Connection error: %s
        """
            % e.reason
        )
    